const puppeteer = require('puppeteer');

const [, , URL, OUTPUT = `${encodeURIComponent(URL)}.pdf`] = process.argv;

(async () => {
  process.stdout.write(`${URL} >> `);
  const browser = await puppeteer.launch({
    headless: true,
    args: ['--no-sandbox', '--disable-setuid-sandbox'],
  });
  const page = await browser.newPage();
  await page.goto(URL, { waitUntil: 'networkidle2' });
  await page.emulateMediaType('print');
  await page.pdf({
    path: OUTPUT,
    format: 'A4',
    margin: { top: '1.2cm' },
  });

  await browser.close();
  process.stdout.write(`${OUTPUT}\n`);
})();
